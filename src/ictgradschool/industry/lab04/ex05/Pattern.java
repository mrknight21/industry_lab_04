package ictgradschool.industry.lab04.ex05;

/**
 * Created by mche618 on 14/03/2017.
 */
public class Pattern {

    private int numberRep;
    private char symbol;

    public Pattern(int numberRep, char symbol)
    {
        this.numberRep= numberRep;
        this.symbol= symbol;
    }

    public int getNumberOfCharacters ()
    {
        return numberRep;
    }

    public String toString()
    {
        String s = "";
        for ( int i =0; i < numberRep; i++)
        {
            s+= symbol;
        };
        return s;
    }
    public void setNumberOfCharacters( int a )
    {
        this.numberRep = a;
    }
}
