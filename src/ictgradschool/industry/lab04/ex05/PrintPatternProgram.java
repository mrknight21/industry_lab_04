package ictgradschool.industry.lab04.ex05;

public class PrintPatternProgram {

    public void start() {
        
        printPatternOne();
        printPatternTwo();
        
    }
    private void printPatternOne() {
        System.out.println("First Pattern");


        Pattern top = new Pattern(15, '*');
        
        Pattern sideOfFirstLine = new Pattern(7, '#');
        Pattern sideOfLine = new Pattern(7, '~');
        Pattern middle = new Pattern(1, '.');
        
        System.out.println(top);
        System.out.println(sideOfFirstLine.toString() + middle.toString() + sideOfFirstLine.toString());
        
        for (int i = 0; i < 6; i++) {
            middle.setNumberOfCharacters(middle.getNumberOfCharacters() + 1);
            System.out.println(sideOfLine.toString() + middle.toString() + sideOfLine.toString());
        }
        
        System.out.println();
    }
    
    private void printPatternTwo() {
        System.out.println("Second Pattern");
        Pattern a = new Pattern(36,'@');
        Pattern b = new Pattern(12, '=');
        Pattern c = new Pattern(12, '.');
        Pattern d = new Pattern(12, '&');
        System.out.println( a.toString());
        System.out.println(b.toString()+c.toString()+b.toString());

        for ( int i =0; i <6; i++)
        {
            d.setNumberOfCharacters(d.getNumberOfCharacters()+1);
            c.setNumberOfCharacters(c.getNumberOfCharacters()-2);
            System.out.println(d.toString()+c.toString()+d.toString());
        }


    }

    public static void main(String[] args) {
        PrintPatternProgram ppp = new PrintPatternProgram();
        ppp.start();
    }
}
