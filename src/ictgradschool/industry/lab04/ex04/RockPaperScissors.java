package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final String[] Choice = { "Rock", "Scissors", "Paper", "Quit"};
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.

    public void start() {
        System.out.print( "Hi! What is your name? ");
        String name = Keyboard.readInput();
        int playerChoice = 0;
        do {
            System.out.println("1. Rock");
            System.out.println("2. Scissors");
            System.out.println("3. Paper");
            System.out.println("4. Quit");
            System.out.print("        Enter choice: ");
            playerChoice = Integer.parseInt(Keyboard.readInput());
            System.out.print(" \n");


            if ( playerChoice == 4)
            {
                System.out.println("Goodbye "+name+". Thanks for playing :)");
                break;
            }
            else if ( playerChoice == 1 ||  playerChoice == 2 || playerChoice == 3 )
            {
            int computerChoice = (int) (Math.random()*3 +1);
            displayPlayerChoice( name, playerChoice);
            System.out.println( "Computer chose "+Choice[computerChoice-1]+".");
            if ( playerChoice == computerChoice)
            {
                System.out.println(getResultString(playerChoice, computerChoice)+".");
                System.out.println(" No one win.");
            }
            else if ( userWins(playerChoice, computerChoice))
            {
                System.out.print(name +" wins because "+ getResultString(playerChoice, computerChoice));
            }
            else{
                System.out.print("Computer wins because "+ getResultString(playerChoice, computerChoice));
            }
            }



            else
            {
                System.out.println( "Invalid Input! Try again!");
            }
            System.out.println(" \n\n");

    }while ( playerChoice != 4);}


    public void displayPlayerChoice(String name, int choice) {
        System.out.println(name+" chose "+Choice[choice-1]+".");
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        if (Choice[playerChoice].equals(Choice[0]))
        {
            if (Choice[computerChoice].equals(Choice[1]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (Choice[playerChoice].equals(Choice[1]))
        {
            if (Choice[computerChoice].equals(Choice[2]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (Choice[playerChoice].equals(Choice[2]))
        {
            if (Choice[computerChoice].equals(Choice[1]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

else return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {
        int total = playerChoice + computerChoice;
        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";
        if ( playerChoice == computerChoice)
        {
            return TIE;
        }
        if (total == 3)
        {
            return ROCK_WINS;
        }
        else if (total == 4)
        {
            return PAPER_WINS;
        }
        else if (total == 5)
        {
            return SCISSORS_WINS;
        }

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        return null;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
