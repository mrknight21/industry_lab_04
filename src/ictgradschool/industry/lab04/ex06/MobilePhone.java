package ictgradschool.industry.lab04.ex06;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    private String brand;
    private String model;
    private double price;

    public MobilePhone(String brand, String model, double price) {
        this.brand= brand;
        this.model= model;
        this.price = price;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here

    public String getModel(){
        return model;
    }
    
    // TODO Insert setModel() method here

    public void setModel(String mod){
        this.model= mod;
    }
    
    // TODO Insert getPrice() method here
    public double getPrice()
    {
        return price;
    }
    
    // TODO Insert setPrice() method here
    public void setPrice( double price)
    {
        this.price= price;
    }

    // TODO Insert toString() method here
    public String toString()
    {
        String s = brand+ " " +model +" "+ price;
        return s;
    }
    
    // TODO Insert isCheaperThan() method here
    public boolean isCheaperThan (MobilePhone a)
    {
        if ( this.price< price)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    // TODO Insert equals() method here
public boolean equals(MobilePhone a)
{
    return( this.price == price & this.brand.equals( brand) & this.model.equals(model)); }
}




