package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {
        int random = (int) (Math.random() * 100 + 1);
        int guess = 0;
        do {
            System.out.print("Enter your guess (1 – 100): ");
            guess = Integer.parseInt(Keyboard.readInput());
            if (guess < 1 || guess > 100) {
                System.out.print("Invalid guess! try again\n");
            } else if (guess == random) {
                System.out.print("Perfect!\n");
                System.out.print("Goodbye\n");
            } else if ( guess < random) {
                System.out.print("Too low, try again\n");
            }
            else if (guess > random)
            {
                System.out.print("Too high, try again\n");
            }


    } while(guess != random);





        /**
         * Program entry point. Do not edit.
         */




    }

    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
