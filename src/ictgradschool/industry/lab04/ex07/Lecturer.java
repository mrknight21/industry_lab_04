package ictgradschool.industry.lab04.ex07;

public class Lecturer {

    // instance variables
    private String name;
    private int staffId;
    private String[] papers;
    private boolean onLeave;
    
    public Lecturer(String name, int staffId, String[] papers, boolean onLeave) {
        this.name = name;
        this.staffId = staffId;
        this.papers = papers;
        this.onLeave = onLeave;
    }
    
    // TODO Insert getName() method here
    public String getName(){ return name;}
    
    // TODO Insert setName() method here
    public void setName( String name){ this.name = name;}
    // TODO Insert getStaffId() method here
    public int getStaffId (){ return staffId; }
    // TODO Insert setStaffId() method here
    public void  setStaffId(int staffId){ this.staffId=staffId;}
    // TODO Insert getPapers() method here
    public String[] getPapers(){ return papers;}
    // TODO Insert setPapers() method here
    public  void setPapers( String [] papers){ this.papers=papers;}
    // TODO Insert isOnLeave() method here
    public 
    // TODO Insert setOnLeave() method here
    
    // TODO Insert toString() method here
    
    // TODO Insert teachesMorePapersThan() method here

}


